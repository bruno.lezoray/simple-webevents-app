# webapp/newsletter/forms.py
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class SubscribeForm(FlaskForm):
    name = StringField("Name", [DataRequired()])
    submit = SubmitField("Subscribe")


class UnsubscribeForm(FlaskForm):
    unsubscribe = SubmitField("Unsubsquirle")